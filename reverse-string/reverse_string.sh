#!/usr/bin/env bash

reverse() {
  local original=$1
  local reversed=""
  local length=${#original}

  for(( index=length-1; index>=0; index-- ))
  do
    reversed+=${original:index:1}
  done

  echo "${reversed}"
}

reverse "$@"
