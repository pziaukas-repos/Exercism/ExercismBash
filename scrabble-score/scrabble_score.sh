#!/usr/bin/env bash

score() {
  local word=${1,,}
  local length=${#word}
  local score_total=0

  for (( index=0; index<length; index++ ))
  do
    letter=${word:index:1}
    score=1

    if [[ "$letter" == [dg] ]]
    then
      score=2
    elif [[ "$letter" == [bcmp] ]]
    then
      score=3
    elif [[ "$letter" == [fhwvy] ]]
    then
      score=4
    elif [[ "$letter" == k ]]
    then
      score=5
    elif [[ "$letter" == [jx] ]]
    then
      score=8
    elif [[ "$letter" == [qz] ]]
    then
      score=10
    fi

    score_total=$(( score_total + score ))
  done

  echo ${score_total}
}

score "$@"
