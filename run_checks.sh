#!/usr/bin/env bash

main() {
  local command=$1
  local shebang_pattern=$2

  for dir in */
  do
    cd ${dir}
    echo "Analyzing '${dir%*/}'..."
    for file in *.sh; do
      if [[ `head -1 ${file}` == ${shebang_pattern} ]]; then
        echo "...'${file}'"
        eval ${command} ${file}
        if [[ $? -ne 0 ]]; then
          exit 1
        fi
      fi
    done
    cd ..
  done
}

main "$@"
