#!/usr/bin/env bash

raindrop_speak() {
  local number=$1
  local translation=""

  if (( number % 3 == 0 ))
  then
    translation="Pling"
  fi
  if (( number % 5 == 0 ))
  then
    translation+="Plang"
  fi
  if (( number % 7 == 0 ))
  then
    translation+="Plong"
  fi

  if [[ ${#translation} -eq 0 ]]
  then
    translation=${number}
  fi

  echo "${translation}"
}

raindrop_speak "$@"
