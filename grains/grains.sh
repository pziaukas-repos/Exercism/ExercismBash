#!/usr/bin/env bash

REQUEST=$1

grains() {
  power=$(($1 - 1))
  echo "2 ^ $power" | bc
}

if [ "$REQUEST" -eq "$REQUEST" ] 2>/dev/null
then
  if [[ ${REQUEST} -gt 0 ]] && [[ ${REQUEST} -lt 65 ]]
  then
    grains "${REQUEST}"
    exit 0
  fi
else
  if [[ ${REQUEST} == "total" ]]
  then
    echo "$(grains 65) - 1" | bc
    exit 0
  fi
fi

echo "Error: invalid input"
exit 1
