#!/usr/bin/env bash

first_letter() {
  local word=$1
  local length=${#word}

  for (( index=0; index<length; index++ ))
  do
    letter=${word:index:1}
    if [[ $letter =~ [[:alpha:]] ]]
    then
      echo "${letter^^}"
      break
    fi
  done

  echo ""
}

acronym() {
  local result=""
  read -r -a words <<< "${1//-/ }"
  for word in "${words[@]}"
  do
    result+=$(first_letter "$word")
  done
  echo "$result"
}

acronym "$@"
