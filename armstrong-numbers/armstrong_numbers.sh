#!/usr/bin/env bash

is_armstrong() {
  local number=$1
  local length=${#number}

  local sum=0
  for (( index=0; index<length; index++ ))
  do
    sum=$(( sum + ${number:$index:1} ** length ))
  done

  if [[ ${number} -eq ${sum} ]]
  then
    echo "true"
  else
    echo "false"
    return 1
  fi
}

is_armstrong "$@"
