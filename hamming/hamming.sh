#!/usr/bin/env bash

hamming_distance() {
  local strand1=$1
  local strand2=$2
  local length=${#strand1}

  local distance=0
  for (( index=0; index<length; index++ ))
  do
    if [[ "${strand1:$index:1}" != "${strand2:$index:1}" ]]
    then
      distance=$(( distance + 1 ))
    fi
  done

  echo ${distance}
}

if [[ $# -eq 2 ]]
then
  if [[ ${#1} -eq ${#2} ]]
  then
    hamming_distance "$@"
    exit 0
  else
    echo "left and right strands must be of equal length"
  fi
else
  echo "Usage: hamming.sh <strand1> <strand2>"
fi
exit 1
