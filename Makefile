.PHONY: lint test
$(VERBOSE).SILENT:

## Inspect the code style using Shellcheck
lint:
	./run_checks.sh shellcheck "#*bash"

## Test the exercises using Bats
test:
	./run_checks.sh bats "#*bats"
