#!/usr/bin/env bash

is_pangram() {
  local text=${1,,}
  for letter in {a..z}; do
    [[ $text = *$letter* ]] || return 1
  done
  return 0
}

if is_pangram "$@"
then
  echo "true"
else
  echo "false"
fi
