#!/usr/bin/env bash

main() {
  local message=${1//[[:space:]]/}
  if [[ -z ${message} ]]; then
    echo "Fine. Be that way!"
  elif [[ ${message} == *[[:upper:]]* && ${message} != *[[:lower:]]* ]]; then
    if [[ ${message: -1} == "?" ]]; then
      echo "Calm down, I know what I'm doing!"
    else
      echo "Whoa, chill out!"
    fi
  elif [[ ${message: -1} == "?" ]]; then
      echo "Sure."
  else
    echo "Whatever."
  fi
}

main "$@"
