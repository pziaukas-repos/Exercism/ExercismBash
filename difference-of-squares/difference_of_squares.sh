#!/usr/bin/env bash

square_of_sum() {
  local n=$1
  echo $(( n**2 * (n + 1)**2 / 4 ))
}

sum_of_squares() {
  local n=$1
  echo $(( n * (n + 1) * (2 * n + 1) / 6 ))
}

if [[ "$1" == "square_of_sum" ]]
then
  square_of_sum "$2"
elif [[ "$1" == "sum_of_squares" ]]
then
  sum_of_squares "$2"
elif [[ "$1" == "difference" ]]
then
  echo $(( $(square_of_sum "$2") - $(sum_of_squares "$2") ))
fi
